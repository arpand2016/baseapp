import { Component, OnInit, Pipe, OnChanges, ViewChild, ElementRef } from '@angular/core';
import {TableMetadata } from '../models/datatype.model';
import { Coltype, FieldType } from '../models/coltype.model';
import { StoreMetadataService } from '../services/store-metadata.service';
import { NgForm } from '@angular/forms';
import { DxDataGridComponent,
  DxDataGridModule,
  DxSelectBoxModule,
  DxCheckBoxModule } from 'devextreme-angular';

// import {csvjson} from 'csvjson';
// import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
// import { DxTreeListModule } from 'devextreme-angular'

@Component({
  selector: 'app-csvbase',
  templateUrl: './csvbase.component.html',
  styleUrls: ['./csvbase.component.scss'],
  providers: [StoreMetadataService]

})
export class CsvbaseComponent implements OnInit {

  @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;
  @ViewChild('tablekey') tablekey: ElementRef;
  @ViewChild('dbdescription') tabledescription: ElementRef;
  @ViewChild('panel') public panel: ElementRef;



  /** for grid */
  saleAmountHeaderFilter: any;
  applyFilterTypes: any;
  currentFilter: any;
  showFilterRow: boolean;
  showHeaderFilter: boolean;
  gridFilterValue: any;
  // fields: {};
  fields: Array<any>;
  filter: any;
  /** -- */
  // nospacepattern = "[^' ']+";
  nospacepattern = '^[-a-zA-Z0-9_\.]+$';
  colSettings = {};
  isShow: boolean;
  exportDataisShow: boolean;
  isShowexportbutton: boolean;
  exportData = {};
  tablemetadata: TableMetadata[];
  datacounter: number;
  csv: any = null;
  // str: Array<Object> = [];
  str: TableMetadata;
  key: string = null;
  filesize: number = null;
  datakey: string;
  isLoading: boolean;

  constructor(private _service: StoreMetadataService) {

    /** for grid */
    this.isShow = false;
    this.exportDataisShow = false;
    this.isShowexportbutton = false;
    this.showFilterRow = true;
    this.showHeaderFilter = true;
    this.applyFilterTypes = [{
            key: 'auto',
            name: 'Immediately'
        }, {
            key: 'onClick',
            name: 'On Button Click'
        }];

        this.currentFilter = this.applyFilterTypes[0].key;
        // this.orderHeaderFilter = this.orderHeaderFilter.bind(this);
        /** -- */
    this.colSettings = { 'colkey': null };
    this.tablemetadata = this._service.getMetadata();
    this.datacounter = 0;
    this.datakey = '';
    this.str = new TableMetadata();
  }

  ngOnInit() {
  }

  fnopensetting(colname) {
    // console.log(colname,'colname..');
    this.exportDataisShow = false;
    console.log(this.colSettings['colname']);

   // if (this.colSettings['colkey'] == null)
   //   this.isShow = true;

    // if(colname == this.colSettings['colkey'])
    this.isShow = !this.isShow;
    this.colSettings = colname;
    // this.colSettings = {'colkey':this.str['colSettings'][colname],'colname':colname, 'type':'varchar', 'length':256};

    console.log(this.colSettings, 'here i am ');
  }

  fnchangecolsettings(event) {
    console.log(event.dataField, 'event edit');
    // const i = this.str['colSettings'].filter(x => x.dataField === event.dataField);
    // console.log(i);
    const index = this.str['colSettings'].findIndex(x => x.dataField === event.dataField);
    // console.log(index, '=index');
    this.str['colSettings'][index] = event;

    // console.log(this.str['colSettings']);
    // this.str['colSettings'][event.colkey] = event;
    console.log(this.str['colSettings'], 'after edit');
  }

  fnFilldata(keydata) {
    console.log(keydata);


    this.datakey = keydata.key;
    this.str = keydata.keydata;
    this.isShow = false;
    /*this.fields = this.str.colSettings;

    this.filter = this.str.filterformat;
    this.gridFilterValue = this.filter;*/

    this.fields = [];
    this.filter = [];

    this.fields = this.str.colSettings;
    /*this.filter = [
      [ this.str['colSettings'][0].dataField, "<>", "" ]
    ];*/
    this.gridFilterValue = this.filter;
    this.filter = this.str.filterformat;
    setTimeout(() => {  // <<<---    using ()=> syntax
      this.gridFilterValue = this.filter;
    }, 1000);

    //
    console.log(this.fields, '..filldata..');
  }

  exportdata(): void {

    /*this.exportData = {
    'tabledefination':  this.str['colSettings'],
    'tabledata':this.str['rows']
    };*/
    const exportmetadataData: TableMetadata = { dataname : this.str['dataname'],
      description : this.str['description'],
      colSettings: this.str['colSettings'],
      header: [], rows: this.str['rows'],
      filterformat: this.gridFilterValue
    };
    // console.log(this.tablekey.nativeElement.value, '=key');
    // console.log(this.tabledescription.nativeElement.value, '=description');

    if ( this.datakey === '') {
      this.datakey = 'key' + this.datacounter;
    }
    //

    this.exportData = this._service.setMetadata(exportmetadataData, this.datakey);
    console.log(this.exportData, 'export data');
    this.exportData = this._service.getMetadata();
    // this.exportData.push(this.str['colSettings']);
    this.exportDataisShow = true;
    setTimeout(() => {
      this.panel.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' }, 500);
    });

  }

  buttonClick() {
    this.gridFilterValue = this.filter;
  }


  csvJSON(csv: string): TableMetadata {

    const lines = csv.split('\n');

    const result = [];
    let colSettings: Array<any>;

    const finalresult = new TableMetadata();

    const headers = lines[0].split (',');

    for (let i = 1; i < 2; i++) {
      const coltype = [];
      // coltype['PID']={ 'colkey': 'PID', 'name': 'ID', 'type': 'integer', 'defination': '11' };
      for (let j = 0; j < headers.length; j++) {
        // coltype[headers[j].replace(" ", "_")] = { 'colkey': headers[j].replace(" ", "_"),
        // 'name': headers[j].replace(" ", "_"), 'type': 'varchar', 'defination': '256' };
        coltype.push({'dataField': headers[j].replace(' ', '_'),
        'caption': headers[j].replace(' ', '_'),
        'dataType': 'string',
        'format': '256' });
      }
      // colSettings.push(coltype);
      colSettings = coltype;
    }

    for (let i = 1; i < lines.length; i++) {
      const obj = {};
      const currentline = lines[i].split(',');
      // obj['PID']= i;
      for (let j = 0; j < headers.length; j++) {
        obj[headers[j].replace(' ', '_')] = currentline[j];
      }
      result.push(obj);
    }
    finalresult['dataname'] = '';
    finalresult['description'] = '';
    finalresult['header'] = headers;
    finalresult['rows'] = result;
    finalresult['colSettings'] = colSettings;

    console.log(finalresult, 'final result');
    return finalresult; // JavaScript object
    // return JSON.stringify(result); //JSON
  }

  fnAddnewdata() {
    console.log('add new data');
    this.str = new TableMetadata();
    this.isShowexportbutton = false;
    this.isShow = false;
    this.filesize = 0;
    this.datakey = '';

  }
  editcol() {
    console.log('Hello');
  }

  onCellClick(e) {
    if (e.rowType === 'header') {
      // your code
      console.log(e);
    }
  }
  /** custom */
  changeListener(files: FileList) {
    this.isLoading = true;
    if (files && files.length > 0) {
      const file: File = files.item(0);
      // console.log(file.name);
      // console.log(file.size);
      // console.log(file.type);
      this.filesize = file.size;
      const reader: FileReader = new FileReader();
      reader.readAsText(file);
      reader.onload = (e) => {
        this.csv = reader.result;
        /*let x = this.csv.split("\n");
        for(let i=0; i<x.length;i++) {
          //this.str+= x[i] + '<br />'
          let row = x[i].split(",");
          this.str[i] = row;
        }*/
        this.str = this.csvJSON(this.csv);
        this.key = this.str['header'][0].replace(' ', '_');
        this.str['dataname'] = file.name.replace(' ', '_');
        this.str['description'] = '';
        // this.str = this.str.splice(0,-1)
        console.log(this.str, this.key);

        // this.str.colSettings.for
        this.fields = this.str.colSettings;

        // this.str['colSettings'][0].dataField;

        // this.fields = getFields();
        this.filter = [
          [ this.str['colSettings'][0].dataField, '<>', '']
        ];
        this.gridFilterValue = this.filter;
      };

    }
    this.isShowexportbutton = true;
    this.isShow = false;
    this.datacounter++;
    // console.log(lines,'show data');
    // var myJsonString = JSON.stringify(lines);
    // console.log(json);
    setTimeout(() => { this.isLoading = false; }, 1000);

  }
  /** --- */

}
