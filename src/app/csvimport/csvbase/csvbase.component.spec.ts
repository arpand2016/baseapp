import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CsvbaseComponent } from './csvbase.component';

describe('CsvbaseComponent', () => {
  let component: CsvbaseComponent;
  let fixture: ComponentFixture<CsvbaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CsvbaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CsvbaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
