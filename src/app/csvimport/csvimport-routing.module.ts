import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CsvbaseComponent} from './csvbase/csvbase.component'

const routes: Routes = [
  { path: 'importcsv',  component: CsvbaseComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CsvimportRoutingModule { }
