import { NgModule, Pipe, PipeTransform } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatCardModule, MatInputModule, MatSnackBarModule, MatToolbarModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CsvimportRoutingModule } from './csvimport-routing.module';
import { CsvbaseComponent } from './csvbase/csvbase.component';
import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
// import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { DxDataGridComponent,  DxDataGridModule,  DxSelectBoxModule,  DxCheckBoxModule, DxButtonModule,
  DxFilterBuilderModule } from 'devextreme-angular';
import { DxTreeListModule } from 'devextreme-angular';
import { EdittypeComponent } from './edittype/edittype.component';
import { TreeviewComponent } from './treeview/treeview.component';

@Pipe({
  name: 'prettyprint'
})
export class PrettyPrintPipe implements PipeTransform {
  transform(val) {
    return JSON.stringify(val, null, 2)
      .replace(/ /g, '&nbsp;')
      .replace(/\n/g, '<br/>');
  }
}



@NgModule({
  imports: [
    CommonModule,
    CsvimportRoutingModule,
    FileUploadModule,
    DxTreeListModule,
    DxDataGridModule,
    DxSelectBoxModule,
    DxCheckBoxModule,
    FormsModule, ReactiveFormsModule,
    MatButtonModule, MatCardModule, MatInputModule, MatSnackBarModule, MatToolbarModule,
    DxButtonModule,
    DxFilterBuilderModule
  ],
  declarations: [CsvbaseComponent, EdittypeComponent, PrettyPrintPipe, TreeviewComponent],
})
export class CsvimportModule { }
