import { Component, OnInit, Input, Output, EventEmitter, OnChanges  } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Coltype, FieldType } from '../models/coltype.model';
@Component({
  selector: 'app-edittype',
  templateUrl: './edittype.component.html',
  styleUrls: ['./edittype.component.scss']
})
export class EdittypeComponent implements  OnChanges {

  nospacepattern = '^[-a-zA-Z0-9_]+$';
  coltypedropdown: string[];
  // coltypes = Coltype;
  // colsettings:any[] = [new ColSettings()]
  initialcolsettings: FieldType;
  successmessageisShow = false;
  isLoading: boolean;
  formatlabel: string;

  @Input() tabledata: FieldType;
  @Output() changecol = new EventEmitter<any>();
  constructor() {
    this.coltypedropdown = ['string', 'number', 'date'];
    // console.log(this.coltypedropdown, 'dropdown');
    this.isLoading = true;
    this.formatlabel = 'Lenght';

  }

  /**
   * ngOnInit(): void {}
   **/
  onChange(selectionval) {
    // console.log(selectionval);
    if (selectionval === 'number') {
      this.initialcolsettings.format = '11';
      this.formatlabel = 'Number Lenght';
    }
    if (selectionval === 'date') {
      this.initialcolsettings.format = 'DD-MM-YYYY';
      this.formatlabel = 'Date Format';
    }
    if (selectionval === 'string') {
      this.initialcolsettings.format = '256';
      this.formatlabel = 'String Lenght';
    }
  }

  ngOnChanges() {
    this.isLoading = true;
    this.formatlabel = 'Lenght';
    this.initialcolsettings = new FieldType;
    // this.initialcolsettings = new FieldType();
    console.log(this.initialcolsettings, 'inital settings');
    // this.initialcolsettings = {}
    // console.log(this.tabledata,'triggered');
    setTimeout(() => {
    this.successmessageisShow = false;
    this.initialcolsettings = this.tabledata;
    this.successmessageisShow = false;
    console.log(this.initialcolsettings, 'triggered');
    this.isLoading = false;
    }, 1000);

  }
  register(initialcolsettings: NgForm) {
    // console.log('triggered autometically');
    // console.log(initialcolsettings.value);
    this.successmessageisShow = true;
    this.changecol.emit(initialcolsettings.value);
  }

}
