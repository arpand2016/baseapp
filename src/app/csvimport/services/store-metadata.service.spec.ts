import { TestBed, inject } from '@angular/core/testing';

import { StoreMetadataService } from './store-metadata.service';

describe('StoreMetadataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StoreMetadataService]
    });
  });

  it('should be created', inject([StoreMetadataService], (service: StoreMetadataService) => {
    expect(service).toBeTruthy();
  }));
});
