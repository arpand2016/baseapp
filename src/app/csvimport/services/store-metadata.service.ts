import { Injectable } from '@angular/core';
import {TableMetadata } from '../models/datatype.model';

// var tablemetadata:tableMetadata;


@Injectable({
  providedIn: 'root'
})
export class StoreMetadataService {
  public tablemetadata: TableMetadata[] = [];
  i = 0;
 // finaldataset = {''};
  constructor() {
    // this.tablemetadata = new tableMetadata();
   }
  getMetadata(): TableMetadata[] {
    return this.tablemetadata;
  }

  setMetadata(dataset: TableMetadata, counter: string): TableMetadata[] {
    const mycounter: string = counter;
    // this.tablemetadata.push(dataset);
    this.tablemetadata[mycounter] = dataset ;
    return this.tablemetadata;
  }





}
