import { CsvimportModule } from './csvimport.module';

describe('CsvimportModule', () => {
  let csvimportModule: CsvimportModule;

  beforeEach(() => {
    csvimportModule = new CsvimportModule();
  });

  it('should create an instance', () => {
    expect(csvimportModule).toBeTruthy();
  });
});
