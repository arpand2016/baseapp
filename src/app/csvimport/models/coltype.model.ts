export class ColSettings {
id: number;
colname: string;
coltype: string;
}

export enum Coltype {
    string = 'string',
    number = 'number',
    date = 'date',
}

export class FieldType {
caption: string;
dataField: string;
dataType: Coltype;
format: any;
}


