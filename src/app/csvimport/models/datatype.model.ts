export class TableMetadata {
    dataname: string;
    description: string;
    colSettings: Array<any>;
    header: Array<any>;
    rows: Array<any>;
    filterformat: any;
}

/*export class tableMetadata {
 [key:string]:{
                colSettings:Object;
                header:any[];
                rows:any[];
              }
}*/
