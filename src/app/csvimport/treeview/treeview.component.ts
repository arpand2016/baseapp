import { Component, OnInit, Input, Output, EventEmitter, OnChanges  } from '@angular/core';

@Component({
  selector: 'app-treeview',
  templateUrl: './treeview.component.html',
  styleUrls: ['./treeview.component.scss']
})
export class TreeviewComponent implements OnChanges {
  treelist: {};
  header: string;
  treeitemShow: boolean;
  selectedIndex: string;

  @Input() treedata: {};
  @Input() treehead: string;
  @Output() addnewdata = new EventEmitter<any>();
  @Output() filldata = new EventEmitter<any>();
  @Output() opensetting = new EventEmitter<any>();

  constructor() { this.treeitemShow = false;
    this.selectedIndex = '';
  }
  fnFilldata(keydata, key: string) {
    // console.log(key);
    // keydata['key'] = key;
    this.filldata.emit({keydata, key});
  }
  fnopensetting(key: Array<any>) {
      this.opensetting.emit(key);
  }

  /* ngOnInit() {
  }*/

  ngOnChanges() {
    this.treelist = this.treedata;
    this.header = this.treehead;
  }
  opentree(index: string) {
    this.selectedIndex = index;
    // this.treeitemShow = !this.treeitemShow;
  }

  fnAddnewdata() {
    console.log('event emmitter triggered');
    this.addnewdata.emit();
  }

}
