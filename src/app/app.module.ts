import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CsvimportModule } from './csvimport/csvimport.module';
import {CsvimportRoutingModule} from './csvimport/csvimport-routing.module';

@NgModule({
  declarations: [
    AppComponent,
        
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CsvimportModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
